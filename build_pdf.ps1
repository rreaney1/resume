$MyInvocation.MyCommand.Path | Split-Path | Push-Location
docker build -t latex:builder .
docker run --name latex-builder latex:builder
docker cp latex-builder:src/output/ .
docker container rm -f latex-builder