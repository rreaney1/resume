FROM pandoc/latex
WORKDIR /src
COPY . /src
RUN mkdir -p output && \
    pdflatex resume.tex && \
    mv -f resume.pdf output/reaney_resume.pdf
ENTRYPOINT ["/bin/sh"]